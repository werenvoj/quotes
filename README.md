#include <stdio.h>
#include <stdlib.h>

#define IN 42
#define CACHE_NO_VALUE -1
/*long long int Factorial(long long int n)
{
    long long int res;
    if (n > 1)
        res = n * Factorial(n - 1);
    else
        res = n;
    return res;
}*/

/*int Fibonacci_naive(int n)
{
    if (n == 0)
        return 0;
    if (n > 2)
        return Fibonacci(n - 1) + Fibonacci(n - 2);
    return 1;
}*/

int FibonacciCalculate(int n, int *cache)
{
    if (cache[n] != CACHE_NO_VALUE)
        return cache[n];
    int res = FibonacciCalculate(n - 1, cache) + FibonacciCalculate(n - 2, cache);
    cache[n] = res;
    return res;
}

int Fibonacci(int n)
{
    int *cache = (int *)malloc((n + 1) * sizeof(int));
    cache[0] = 0;
    cache[1] = 1;
    for (int i = 2; i <= n; i++)
        cache[i] = CACHE_NO_VALUE;
    int res = FibonacciCalculate(n, cache);
    free(cache);
    return res;
}

int main()
{
    // printf("%lld\n", Factorial(20));
    printf("%d\n", Fibonacci(IN));
    return 0;
}
